"use strict"; // Operate in Strict mode

// loads config file

var config = require('./config');

// nodejs modules
const mkdirp = require('mkdirp');
const http = require('http');
const url = require('url');
const sharp = require('sharp');
const fs = require('fs');
var path = require('path');


var Resize = (function (imageObject, res) {

    var height = parseInt(imageObject.height, 10);

    if (isNaN(height)) {
      height = null;
      console.log('isnan height' + height)
    }

    var width = parseInt(imageObject.width, 10)
    if (isNaN(width)) {
      width = null;

    }

    // creates Folder and then resizes
    console.log(imageObject.path);
    mkdirp(imageObject.path, function (err) {
      if (err) console.error(err)
      else {

        // Beginn Resizing
        if (imageObject.method == 'thumb') {
          console.log('thumb image will be created');
          sharp(imageObject.src)
            .resize(width, height)
            .max()
            .toFile(imageObject.dest, function (err) {
              if (err != null) {
                console.log(err);
                console.log(imageObject.id + ' doesn\'t exist in the src folder');
                res.setHeader('Content-Type', 'text/plain');
                res.statusCode = 404;
                res.end('Not found');
              } else {
                console.log('Image created: ' + imageObject.dest);
                if (res != null) {
                  var image = fs.createReadStream(imageObject.dest);
                  res.setHeader('Content-Type', 'image/jpeg');
                  image.pipe(res);
                }
              }
          });
        }

        if (imageObject.method == 'resize') {
          console.log('thumb image will be created with ' + imageObject.method);
          sharp(imageObject.src)
            .resize(width, height)
            .toFile(imageObject.dest, function (err) {
              if (err != null) {
                console.log('could not write Image to Disk. ' + err)
                res.setHeader('Content-Type', 'text/plain');
                res.statusCode = 404;
                res.end('Not found');
              } else {
                console.log('Image created: ' + imageObject.dest);
                if (res != null) {
                  var image = fs.createReadStream(imageObject.path + '/' + imageObject.id);
                  res.setHeader('Content-Type', 'image/jpeg');
                  image.pipe(res);
                }
              }
            });
        }
        
      } // End Resizing
    });  
}); 
  
  

var ResizeAll = (function () {
    var files = fs.readdirSync(config.src);
    console.log('hello');
    for (var i = 0; i < files.length; i++) {
      let file = files[i];
      console.log (file);
      for (var e = 0; e < Object.keys(config.imageSizes).length; e++) {
        var imageObject = {}
        imageObject.method = 'thumb'; // thumb
        imageObject.path = config.cache + '/' + imageObject.method + '/' + Object.keys(config.imageSizes)[e]; // cache/thumb/cover/
        imageObject.id = file; // xyz
        imageObject.size = Object.keys(config.imageSizes)[e]; // header
  
        imageObject.dest = imageObject.path + '/' + imageObject.id;
        imageObject.src = config.src + '/' + imageObject.id;
        imageObject.width = config.imageSizes[Object.keys(config.imageSizes)[e]][0];
        imageObject.height = config.imageSizes[Object.keys(config.imageSizes)[e]][1];
  
        // Resize with method thumb
        Resize(imageObject);
        var obj2 = JSON.parse(JSON.stringify(imageObject));
        obj2.method = 'resize'; // resize
        obj2.path = config.cache + '/' + obj2.method  + '/' + Object.keys(config.imageSizes)[e]; // cache/thumb/cover/
        obj2.dest = obj2.path + '/' + imageObject.id;
  
        console.log('imageObject.path: '+ imageObject.path)
        // Resize the same ImageObject with method resize
        Resize(obj2);
      }
    }

    
  })();

  