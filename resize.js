"use strict"; // Operate in Strict mode

// loads config file

var config = require('./config');

// nodejs modules
const mkdirp = require('mkdirp');
const http = require('http');
const url = require('url');
const sharp = require('sharp');
const fs = require('fs');
var path = require('path');

// var gResizer = gResizer || {}; // initialize the variable while ensuring it is not redefined

//// ***************************************************
//// gReziser.Core Function
//// ***************************************************

var gResizer = (function () {

  mkdirp(config.cache, function (err) {
    if (err) console.error(err)
  });
  mkdirp(config.src, function (err) {
    if (err) console.error(err)
  });

  var Server = http.createServer(function (req, res) {
    // Get only the pathname of the URL
    var pathname = url.parse(req.url).pathname;     
    var imageObject = URLHandler(pathname);
    var image = fs.createReadStream(imageObject.dest);

    image.on('open', function () {
      res.setHeader('Content-Type', 'image/jpeg');
      image.pipe(res);
    });
    console.log('imageObject.id: ' + imageObject.id)
    image.on('error', function () {
      console.log('image is not in cache');
      console.log('Resizing: ' + imageObject.id);
      Resize(imageObject, res); 
    });

  });
  
  Server.listen(config.port, function () {
    console.log('Listening on http://' + config.host + ':' + config.port + '/');
    
  });

  var URLHandler = (function (pathname) {

    var parameter = pathname.split("/"); // thumb/cover/xyz

    var imageObject = {}

      imageObject.path =  config.cache + '/' + parameter[1] + '/' +  parameter[2]; // cache/thumb/cover/
      imageObject.id = parameter[3] + '.jpg'; // xyz
      imageObject.method = parameter[1]; // thumb
      imageObject.size = parameter[2]; // header

      imageObject.dest = imageObject.path + '/' + imageObject.id;
      imageObject.src = config.src + '/' + imageObject.id;
      imageObject.width = config.imageSizes[imageObject.size][0];
      imageObject.height = config.imageSizes[imageObject.size][1];
    
    return imageObject;
  });

  var Resize = (function (imageObject, res) {

    var height = parseInt(imageObject.height, 10);

    if (isNaN(height)) {
      height = null;
      console.log('isnan height' + height)
    }

    var width = parseInt(imageObject.width, 10)
    if (isNaN(width)) {
      width = null;

    }

    // creates Folder and then resizes
    console.log(imageObject.path);
    mkdirp(imageObject.path, function (err) {
      if (err) console.error(err)
      else {

        // Beginn Resizing
        if (imageObject.method == 'thumb') {
          console.log('thumb image will be created');
          sharp(imageObject.src)
            .resize(width, height)
            .max()
            .toFile(imageObject.dest, function (err) {
              if (err != null) {
                console.log(err);
                console.log(imageObject.id + ' doesn\'t exist in the src folder');
                res.setHeader('Content-Type', 'text/plain');
                res.statusCode = 404;
                res.end('Not found');
              } else {
                console.log('Image created: ' + imageObject.dest);
                if (res != null) {
                  var image = fs.createReadStream(imageObject.dest);
                  res.setHeader('Content-Type', 'image/jpeg');
                  image.pipe(res);
                }
              }
          });
        }

        if (imageObject.method == 'resize') {
          console.log('thumb image will be created with ' + imageObject.method);
          sharp(imageObject.src)
            .resize(width, height)
            .toFile(imageObject.dest, function (err) {
              if (err != null) {
                console.log('could not write Image to Disk. ' + err)
                res.setHeader('Content-Type', 'text/plain');
                res.statusCode = 404;
                res.end('Not found');
              } else {
                console.log('Image created: ' + imageObject.dest);
                if (res != null) {
                  var image = fs.createReadStream(imageObject.path + '/' + imageObject.id);
                  res.setHeader('Content-Type', 'image/jpeg');
                  image.pipe(res);
                }
              }
            });
        }
        
      } // End Resizing
    });   
  });

  var hello = (function (string) {
    console.log(string);
  });

});
console.log(gResizer);
gResizer.hello('Hallo');
module.exports = gResizer;
