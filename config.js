var config = {};

config.src = "./src"; 
config.cache ="./cache";
config.imageSizes = {
    "cover":["200", "400"],
    "banner":["400", "150"],
    "header":["1920", "null"]
}

//var imgSizes = [thumbCover, thumbBanner, resizeHeader];

config.port = 3200;
config.host = 'localhost';

module.exports = config;
console.log("config.js executed");

