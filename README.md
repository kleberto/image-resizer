## Usecase
Server übergibt Bilder nach Größen & falls das Bild in dieser Größe nicht existiert, erstellt er es.

Der Server erwartet folgende pathnames der URL
```
www.example.com/resizemethod/size/id
```
Ein Beispiel:
```
www.example.com/thumb/cover/101
```
Hier wird 101.jpg aufgerufen in der Größe cover (Pixelgrößen siehe config) mit der Methode thumb (crop).

**ResizeMethods:** thumb, resize

## Installation

Repo Klonen und npm install ausführen
```
npm install
```

## Configuration
Config.js Bildnamen editieren und Größen eintragen.
Falls eine Größe angegeben ist, geht Image Resizer davon aus, dass es die Breite ist.

## Run 
Server startet mit
```
node index.js
```

und einmalig alle Bilder in src konvertieren
```
node resizeall.js
```
